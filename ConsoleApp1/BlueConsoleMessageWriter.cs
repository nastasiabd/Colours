﻿namespace ConteinerDemo.BlueWriter
{
    public class BlueConsoleMessageWriter : IMessageWriter
    {
        public void Write(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
        }
    }
}