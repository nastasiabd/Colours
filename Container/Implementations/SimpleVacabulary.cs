﻿using ContainerDemo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContainerDemo.Implementations
{
    class SimpleVacabulary : IVocabulary
    {
        private Dictionary<Language, string> _voc = new Dictionary<Language, string>
        {
              {Language.eng, "Hello" }
            , {Language.esp, "Salut"}
            , {Language.rus, "Привет"}
            , {Language.ita, "Ciao" }
        };

        public string GetTranslatedSalutationText(Language lang)
        => _voc.Keys.Contains(lang) 
            ? _voc[lang]
            : throw new InvalidOperationException("Перевод не зарегистрирован в словаре");
    }
}
