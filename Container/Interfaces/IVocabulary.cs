﻿namespace ContainerDemo.Interfaces
{
    public interface IVocabulary
    {
        string GetTranslatedSalutationText(Language lang);
    }
}
