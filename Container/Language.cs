﻿namespace ContainerDemo
{
    public enum Language
    {
        rus
        , eng
        , esp
        , ita
    }
}