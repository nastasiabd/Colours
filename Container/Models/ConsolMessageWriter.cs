﻿using ContainerDemo.Contracts;
using System;

namespace ContainerDemo.Models
{
    class ConsolMessageWriter:IMessageWriter
    {
        public void Write(string message)
        {
            Console.WriteLine(message);
            Console.ReadLine();
        }
    }
}
