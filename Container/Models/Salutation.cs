﻿using ContainerDemo.Contracts;
using ContainerDemo.Interfaces;
using System;

namespace ContainerDemo.Models
{
   public class Salutation
    {
        private readonly IMessageWriter _writer;
        private readonly IVocabulary _vocabulary;

        public Salutation(IMessageWriter writer, IVocabulary vocabulary)
        {
            _writer =  writer 
                ?? throw new ArgumentNullException(nameof(writer));

            _vocabulary = vocabulary 
                ?? throw new ArgumentNullException(nameof(vocabulary));
        }

        public void Exclaim(Language lang)
        {
            _writer.Write(_vocabulary.GetTranslatedSalutationText(lang));
        }
    }
}
