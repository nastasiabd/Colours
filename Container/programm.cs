﻿using ContainerDemo.Contracts;
using ContainerDemo.Implementations;
using ContainerDemo.Interfaces;
using ContainerDemo.Models;
using ContainerDemo.RedWriter;
using ContainerDemo.YellowWriter;
using ConteinerDemo.GreenWriter;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ContainerDemo
{
    class Programm
    {
        static void Main(string[] args)
        {
            //Формирование инфраструктуры

            ServiceCollection services = new ServiceCollection();

            //Установка зависимостей(Конфигурирование)
            services.AddTransient<Salutation>();

            services.AddSingleton<IMessageWriter, RedConsoleMessageWriter>();
            services.AddSingleton<IMessageWriter, GreenConsoleMessageWriter>();
            services.AddSingleton<IMessageWriter, YellowConsoleMessageWriter>();

            services.AddSingleton<IVocabulary, SimpleVacabulary>();
 
            //Формирование контейнера

            ServiceProvider container = services.BuildServiceProvider();

            //Работа "Приложения" 

            Salutation salutation = container.GetRequiredService<Salutation>();

            salutation.Exclaim(Language.ita);

            Console.ReadLine();
        }
    }
}
