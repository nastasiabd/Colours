﻿using ContainerDemo.Contracts;
using System;

namespace ContainerDemo.BlueWriter
{
    public class BlueConsoleMessageWriter : IMessageWriter
    {
        public void Write(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
        }
    }
}
