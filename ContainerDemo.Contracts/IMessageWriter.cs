﻿namespace ContainerDemo.Contracts
{
    public interface IMessageWriter
    {
        void Write(string message);
    }
}
