﻿using ContainerDemo.Contracts;
using System;

namespace ContainerDemo.RedWriter
{
    public class RedConsoleMessageWriter : IMessageWriter
    {
        public void Write(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
        }
    }
}
