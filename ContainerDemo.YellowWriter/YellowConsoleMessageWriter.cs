﻿using ContainerDemo.Contracts;
using System;

namespace ContainerDemo.YellowWriter
{
    public class YellowConsoleMessageWriter:IMessageWriter
    {
        public void Write(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
        }
    }
}
