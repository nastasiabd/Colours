﻿using ContainerDemo.Contracts;
using System;

namespace ConteinerDemo.GreenWriter
{
    public class GreenConsoleMessageWriter : IMessageWriter
    {
        public void Write(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
        }
    }
}
